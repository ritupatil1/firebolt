#include "periodic_callbacks.h"
#include "board_io.h"
#include "can_bus.h"
#include "can_bus_initializer.h"
#include "can_motor_mesg_handler.h"
//#include "can_mesg_handler_dbc_sender.h"
//#include "can_mia_confirgurations.h"
#include "gpio.h"
#include "motor_logic.h"
#include "project.h"
#include "speed_sensor.h"
#include <stdio.h>
//#include "sender_receiver.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */

#define baud_rate_kbps 100
#define rx_qSize 100
#define tx_qSize 100
dbc_DRIVER_TO_MOTOR_s dummy_steer_data = {.DRIVER_TO_MOTOR_direction = -2, .DRIVER_TO_MOTOR_speed = 4};

void periodic_callbacks__initialize(void) {

  can_bus_initializer(baud_rate_kbps, can1, tx_qSize, rx_qSize);
  initialize_speed_sensor_interrupts();
  motor_init_pwm();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  if (callback_count < 30) {
    pwm1__set_duty_cycle(PWM_MOTOR, PWM_NEUTRAL);
  }
  if (0 == callback_count % 5) {
    clear_rotations();
  }
  can_handler__handle_all_incoming_messages(can1);
  // can_handler__transmit_messages();
}

void periodic_callbacks__20Hz(uint32_t callback_count) {}

void periodic_callbacks__100Hz(uint32_t callback_count) {}

void periodic_callbacks__1000Hz(uint32_t callback_count) {}