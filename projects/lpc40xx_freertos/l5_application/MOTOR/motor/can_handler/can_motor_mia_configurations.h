#pragma once
#include "project.h"
// -----------------------------------------------------------------------------
// When a message's MIA counter reaches this value
// corresponding MIA replacements occur
// -----------------------------------------------------------------------------
// const uint32_t dbc_mia_threshold_DRIVER_HEARTBEAT;
// const uint32_t dbc_mia_threshold_SENSOR_SONARS;
// const uint32_t dbc_mia_threshold_DESTINATION_LOCATION = 1000;
const uint32_t dbc_mia_threshold_DRIVER_TO_MOTOR = 1000;
// const uint32_t dbc_mia_replacement_MOTOR_SPEED;
// const uint32_t dbc_mia_threshold_GEO_CONTROLLER_COMPASS = 1000;

// -----------------------------------------------------------------------------
// User must define these externed instances in their code to use MIA functions
// These are copied during dbc_service_mia_*() when message MIA timeout occurs
// -----------------------------------------------------------------------------
// const dbc_DRIVER_HEARTBEAT_s     dbc_mia_replacement_DRIVER_HEARTBEAT; // Suggested MIA threshold: (3*None)
// const dbc_SENSOR_SONARS_s        dbc_mia_replacement_SENSOR_SONARS; // Suggested MIA threshold: (3*None)
// const dbc_DESTINATION_LOCATION_s dbc_mia_replacement_DESTINATION_LOCATION; // Suggested MIA threshold: (3*None)
const dbc_DRIVER_TO_MOTOR_s dbc_mia_replacement_DRIVER_TO_MOTOR; // Suggested MIA threshold: (3*None)
const dbc_MOTOR_SPEED_s dbc_mia_replacement_MOTOR_SPEED;         // Suggested MIA threshold: (3*None)
// const dbc_GEO_CONTROLLER_COMPASS_s dbc_mia_replacement_GEO_CONTROLLER_COMPASS; // Suggested MIA threshold: (3*None