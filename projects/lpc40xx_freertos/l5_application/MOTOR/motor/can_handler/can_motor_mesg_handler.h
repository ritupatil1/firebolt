#pragma once
#include "can_bus.h"

void can_handler__manage_mia_10hz(void);

void can_handler__transmit_messages(void);

void can_handler__handle_all_incoming_messages(can__num_e can);
