#include "board_io.h"
#include "can_bus.h"
#include "can_bus_initializer.h"
#include "gpio.h"
#include <stddef.h>

void can_bus_initializer(uint32_t baud_rate, can__num_e can, uint16_t tx_size, uint16_t rx_size) {

  can__init(can, baud_rate, rx_size, tx_size, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can);
  return;
}