#include "board_io.h"
#include "can_bus.h"
#include "can_motor_mia_configurations.h"
#include "gpio.h"
#include "motor_logic.h"
#include "project.h"
#include <stddef.h>
#include <stdio.h>

#define motor 1

bool dbc_send_can_message(void *argument, uint32_t message_id, const uint8_t bytes[8], uint8_t dlc) {
  can__msg_t msg = {};
  memset(msg.data.bytes, 0, 8);
  msg.frame_fields.data_len = dlc;
  msg.msg_id = message_id;
  memcpy(msg.data.bytes, bytes, 8);

  return can__tx(can1, &msg, 0);
}

dbc_DRIVER_TO_MOTOR_s steer_data = {};
static dbc_DRIVER_TO_MOTOR_s mia_val;
void can_handler__manage_mia_10hz(void) {
  const uint32_t mia_increment_value = 100;
  if (dbc_service_mia_DRIVER_TO_MOTOR(&mia_val, mia_increment_value)) {
  }
}

// We are assuming that we have a 10hz function in which we wish
// to transmit all messages that should be sent at 10x per second
void can_handler__transmit_messages(void) {
  dbc_MOTOR_SPEED_s motor_speed = {};
  can__msg_t can_transmit_msg = {};
  motor_speed.MOTOR_SPEED_info = motor_speed_with_direction();
  motor_speed.MOTOR_SPEED_pwm = get_pwm_forward();
  if (dbc_encode_and_send_MOTOR_SPEED(NULL, &motor_speed)) {
    gpio__toggle(board_io__get_led0());
    // printf("MOTOR_SPEED_pwm:%f, MOTOR_SPEED_info: %f\n ", motor_speed.MOTOR_SPEED_pwm, motor_speed.MOTOR_SPEED_info);
  }
}

void can_handler__handle_all_incoming_messages(can__num_e can) {
  can__msg_t can_msg = {};
  bool message_received = false;
  while (can__rx(can, &can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };

    if (dbc_decode_DRIVER_TO_MOTOR(&steer_data, header, can_msg.data.bytes)) {
      gpio__toggle(board_io__get_led1());
      motor_direction(steer_data.DRIVER_TO_MOTOR_direction);
      motor_speed(steer_data.DRIVER_TO_MOTOR_speed);
    }
    return;
  }
}
