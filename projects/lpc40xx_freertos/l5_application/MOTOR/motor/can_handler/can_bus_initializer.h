#pragma once

#include "can_bus.h"

void can_bus_initializer(uint32_t baud_rate, can__num_e can, uint16_t tx_size, uint16_t rx_size);