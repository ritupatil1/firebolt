#pragma once

#include "project.h"
#include "pwm1.h"

#define PWM_MOTOR PWM1__2_1
#define PWM_SERVO PWM1__2_0

static const float PWM_FORWARD_DEFAULT_LOW = 15.2f;
static const float PWM_FORWARD_DEFAULT_HIGH = 18.0f;
static const float PWM_NEUTRAL = 15.0f;
static const float PWM_REVERSE = 14.07f;
static const float PWM_FULL_REVERSE = 10;
static const float TURN_LEFT_90_DEGREES = 11.01;
static const float TURN_RIGHT_90_DEGREES = 18.99;
static const float TURN_LEFT_20_DEGREES = 12.5;
static const float TURN_RIGHT_20_DEGREES = 17.5;
static const float STEER_STRAIGHT = 15.56;

void motor_init_pwm(void);
void motor_logic(dbc_DRIVER_TO_MOTOR_s *steer_data);
void motor_direction(DRIVER_TO_MOTOR_direction_e motor_steer);
float motor_maintain_speed();
void motor_speed(uint8_t motor_speed);
void motor_rc_car_stop_state(void);
float motor_speed_with_direction(void);
float get_pwm_forward(void);
float get_current_speed_mph();
