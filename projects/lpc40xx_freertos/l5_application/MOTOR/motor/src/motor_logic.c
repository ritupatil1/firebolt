#include "motor_logic.h"
#include "board_io.h"
#include "delay.h"
#include "gpio.h"
#include "speed_sensor.h"
#include <stdio.h>

static uint32_t reverse_counter = 0;
static bool reverse_flag = 0;

static float pwm_forward = 15.8;

static float target_speed;
static int8_t current_speed;

void motor_init_pwm(void) {
  gpio__construct_as_output(GPIO__PORT_2, 0);
  gpio__construct_as_output(GPIO__PORT_2, 1);
  gpio__construct_with_function(GPIO__PORT_2, 0, GPIO__FUNCTION_1);
  gpio__construct_with_function(GPIO__PORT_2, 1, GPIO__FUNCTION_1);
  pwm1__init_single_edge(100);
}

void motor_rc_car_stop_state(void) {
  motor_direction(MOTOR_direction_STRAIGHT);
  motor_speed(0);
}

static void motor_dc_forward(uint8_t motor_speed) {
  reverse_counter = 0;
  reverse_flag = false;
  current_speed = motor_speed;
  switch (current_speed) {
  case 4:
    target_speed = 15.89;
    break;
  case 3:
    target_speed = 15.86;
    break;
  default:
    target_speed = 15.89;
    break;
  }

  float pwm = motor_maintain_speed();
  printf("pwm = %f\n", pwm);
  pwm1__set_duty_cycle(PWM_MOTOR, pwm);

  reverse_flag = false;
}

static void motor_dc_reverse(uint8_t motor_speed) {
  pwm_forward = PWM_FORWARD_DEFAULT_LOW;
  reverse_flag = true;
  if (reverse_counter <= 10) {
    pwm1__set_duty_cycle(PWM_MOTOR, PWM_NEUTRAL);
  } else if (reverse_counter <= 35) {
    pwm1__set_duty_cycle(PWM_MOTOR, 14.54);
  }
  reverse_counter++;
}

static void motor_dc_stop(uint8_t motor_speed) {
  pwm_forward = PWM_FORWARD_DEFAULT_LOW;
  reverse_counter = 0;
  reverse_flag = false;

  pwm1__set_duty_cycle(PWM_MOTOR, PWM_NEUTRAL);
}

void motor_speed(uint8_t motor_speed) {
  if (motor_speed == 0) {
    motor_dc_stop(motor_speed);
  } else if (motor_speed > 1) {
    motor_dc_forward(motor_speed);
  } else if (motor_speed == 1) {
    motor_dc_reverse(motor_speed);
  }
}

void motor_direction(DRIVER_TO_MOTOR_direction_e motor_steer) {
  switch (motor_steer) {
  case MOTOR_direction_HARD_LEFT:
    pwm1__set_duty_cycle(PWM_SERVO, TURN_LEFT_90_DEGREES);
    break;
  case MOTOR_direction_SOFT_LEFT:
    pwm1__set_duty_cycle(PWM_SERVO, TURN_LEFT_20_DEGREES);
    break;
  case MOTOR_direction_STRAIGHT:
    pwm1__set_duty_cycle(PWM_SERVO, STEER_STRAIGHT);
    break;
  case MOTOR_direction_SOFT_RIGHT:
    pwm1__set_duty_cycle(PWM_SERVO, TURN_RIGHT_20_DEGREES);
    break;
  case MOTOR_direction_HARD_RIGHT:
    pwm1__set_duty_cycle(PWM_SERVO, TURN_RIGHT_90_DEGREES);
    break;
  default:
    break;
  }
}

float motor_speed_with_direction(void) {
  float speed = get_kmph();
  if (reverse_flag) {
    speed *= -1;
  }
  return speed;
}

float motor_maintain_speed() {
  float speed = get_kmph();

  static uint16_t wait_count = 50;
  if ((pwm_forward - target_speed) > 1.5f) { // Brake logic
    pwm_forward = PWM_FORWARD_DEFAULT_LOW;
    if (reverse_flag == true) {
      wait_count = 0;
    }
    if (wait_count < 15) {
      wait_count++;
      return pwm_forward;
    }
    return 10.0f;
  }

  if ((pwm_forward < target_speed) || (speed < current_speed)) {
    pwm_forward += 0.005f;
  } else if ((pwm_forward > target_speed) || (speed > current_speed)) {
    pwm_forward -= 0.01f;
  }

  if (pwm_forward > PWM_FORWARD_DEFAULT_HIGH) {
    pwm_forward = PWM_FORWARD_DEFAULT_HIGH;
  } else if (pwm_forward < PWM_FORWARD_DEFAULT_LOW) {
    pwm_forward = PWM_FORWARD_DEFAULT_LOW;
  }

  return pwm_forward;
}

float get_pwm_forward(void) { return pwm_forward; }
