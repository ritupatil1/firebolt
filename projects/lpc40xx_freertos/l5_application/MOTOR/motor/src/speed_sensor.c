#include "speed_sensor.h"

#include <stdio.h>

#include "board_io.h"
#include "gpio.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"

static const float wheel_diameter_in_m = 0.1;
static const float kmph_const = 0.1885;

static uint32_t rotations = 0;
static uint32_t rpm;
static float kmph = 0.0f;

void interrupt_handle(void) {
  if (LPC_GPIOINT->IO0IntStatR & (1 << 6)) {
    rotations++;
    fprintf(stderr, " intrr 6 rotations = %ld\n", rotations);
    LPC_GPIOINT->IO0IntClr = (1 << 6);
  }
  if (LPC_GPIOINT->IO0IntStatR & (1 << 29)) {
    rotations++;
    // fprintf(stderr, "rotations = %ld\n", rotations);
    LPC_GPIOINT->IO0IntClr = (1 << 29);
  }
}

void initialize_speed_sensor_interrupts(void) {
  gpio__construct_as_input(0, 6);
  LPC_GPIOINT->IO0IntEnR |= 1 << 6;
  gpio__construct_as_input(0, 29);
  LPC_GPIOINT->IO0IntEnR |= 1 << 29;
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, interrupt_handle, NULL);
}

float get_kmph(void) {
  printf("get_Kmph\n");
  return kmph;
}

uint32_t get_rpm(void) { return rpm; }

void calculate_rpm_and_kmph(uint8_t rotation_count) {
  rpm = (rotation_count * 120);
  kmph = (kmph_const * rpm * wheel_diameter_in_m);
  printf("kmph = %f\n", kmph);
}

void clear_rotations(void) {
  calculate_rpm_and_kmph(rotations);
  rotations = 0;
}
