#pragma once

#include <stdint.h>

void initialize_speed_sensor_interrupts(void);
uint32_t get_rpm(void);
float get_kmph(void);
void clear_rotations(void);
