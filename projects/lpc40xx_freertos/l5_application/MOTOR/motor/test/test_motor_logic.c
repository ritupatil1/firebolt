#include "unity.h"

#include <stdbool.h>

#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockdelay.h"
#include "Mockgpio.h"
#include "Mockpwm1.h"
#include "Mockspeed_sensor.h"

#include "motor_logic.c"
#include "project.h"

void setUp(void) {}

void tearDown(void) {}

void test_init_pwm(void) {
  gpio_s pwm1, pwm2;
  gpio__construct_as_output_ExpectAndReturn(GPIO__PORT_2, 0, pwm1);
  gpio__construct_as_output_ExpectAndReturn(GPIO__PORT_2, 1, pwm2);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_2, 0, GPIO__FUNCTION_1, pwm1);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_2, 1, GPIO__FUNCTION_1, pwm2);
  pwm1__init_single_edge_Expect(100);
  motor_init_pwm();
}

void test_motor_direction(void) {

  DRIVER_TO_MOTOR_direction_e motor_steer = -2;
  pwm1__set_duty_cycle_Expect(PWM_SERVO, TURN_LEFT_90_DEGREES);
  motor_direction(motor_steer);

  motor_steer = -1;
  pwm1__set_duty_cycle_Expect(PWM_SERVO, TURN_LEFT_20_DEGREES);
  motor_direction(motor_steer);

  motor_steer = 0;
  pwm1__set_duty_cycle_Expect(PWM_SERVO, STEER_STRAIGHT);
  motor_direction(motor_steer);

  motor_steer = 1;
  pwm1__set_duty_cycle_Expect(PWM_SERVO, TURN_RIGHT_20_DEGREES);
  motor_direction(motor_steer);

  motor_steer = 2;
  pwm1__set_duty_cycle_Expect(PWM_SERVO, TURN_RIGHT_90_DEGREES);
  motor_direction(motor_steer);
}

void test_control_motor_STOP(void) {
  int16_t speed = 0;
  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 15);

  motor_speed(speed);
  TEST_ASSERT_EQUAL_INT32(0, reverse_counter);
  TEST_ASSERT_FALSE(reverse_flag);
  TEST_ASSERT_EQUAL_FLOAT(15.2, pwm_forward);
}

void test_control_motor_forward_direction_current_speed_greater_than_target_speed(void) {
  int16_t target_speed = 2;
  pwm_forward = 15.9;

  get_kmph_ExpectAndReturn(3);
  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 15.89);

  motor_speed(target_speed);
  TEST_ASSERT_EQUAL_INT32(0, reverse_counter);
  TEST_ASSERT_FALSE(reverse_flag);
  TEST_ASSERT_EQUAL_FLOAT(15.89, pwm_forward);
}

void test_braking_when_current_speed_is_greater_than_target_speed(void) {
  int16_t target_speed = 2;
  pwm_forward = 16.1;

  get_kmph_ExpectAndReturn(4);
  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 16.09);

  motor_speed(target_speed);
  TEST_ASSERT_EQUAL_INT32(0, reverse_counter);
  TEST_ASSERT_FALSE(reverse_flag);
  TEST_ASSERT_EQUAL_FLOAT(16.09, pwm_forward);

  pwm_forward = 16;
  get_kmph_ExpectAndReturn(3.4);
  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 15.99);

  motor_speed(target_speed);
  TEST_ASSERT_EQUAL_INT32(0, reverse_counter);
  TEST_ASSERT_FALSE(reverse_flag);
  TEST_ASSERT_EQUAL_FLOAT(15.99, pwm_forward);
}

void test_control_motor_forward_direction_limit_pwm_when_reaches_max(void) {
  int16_t target_speed = 1;
  pwm_forward = 18;
  get_kmph_ExpectAndReturn(0.5);
  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 15);

  motor_speed(target_speed);
  TEST_ASSERT_EQUAL_INT32(1, reverse_counter);
  TEST_ASSERT_FALSE(reverse_flag);
  TEST_ASSERT_EQUAL_FLOAT(18, pwm_forward);
}

void test_control_motor_forward_direction_limit_pwm_when_reaches_min(void) {
  int16_t target_speed = 1;
  pwm_forward = 15.8;

  get_kmph_ExpectAndReturn(2);
  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 15);

  motor_speed(target_speed);
  TEST_ASSERT_EQUAL_INT32(2, reverse_counter);
  TEST_ASSERT_FALSE(reverse_flag);
  TEST_ASSERT_EQUAL_FLOAT(15.8, pwm_forward);
}

void test_control_motor_reverse_direction(void) {
  uint16_t speed = -1;
  get_kmph_ExpectAndReturn(-1);
  TEST_ASSERT_EQUAL_INT32(2, reverse_counter);

  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 10);
  motor_speed(speed);
  TEST_ASSERT_EQUAL_INT32(1, reverse_counter);
  TEST_ASSERT_TRUE(reverse_flag);

  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 10);
  motor_speed(speed);
  TEST_ASSERT_EQUAL_INT32(2, reverse_counter);

  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 10);
  motor_speed(speed);
  TEST_ASSERT_EQUAL_INT32(3, reverse_counter);

  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 10);
  motor_speed(speed);
  TEST_ASSERT_EQUAL_INT32(4, reverse_counter);

  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 15);
  motor_speed(speed);
  TEST_ASSERT_EQUAL_INT32(5, reverse_counter);

  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 15);
  motor_speed(speed);
  TEST_ASSERT_EQUAL_INT32(6, reverse_counter);

  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 14.07);

  motor_speed(speed);
  TEST_ASSERT_EQUAL_INT32(7, reverse_counter);

  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 14.07);

  motor_speed(speed);
  TEST_ASSERT_EQUAL_INT32(8, reverse_counter);

  for (int i = 0; i < 100; i++) {
    pwm1__set_duty_cycle_Expect(PWM_MOTOR, 14.07);
    motor_speed(speed);
  }

  TEST_ASSERT_EQUAL_INT32(108, reverse_counter);

  speed = 0;
  pwm1__set_duty_cycle_Expect(PWM_MOTOR, 15);

  motor_speed(speed);
  TEST_ASSERT_EQUAL_INT32(0, reverse_counter);
  TEST_ASSERT_FALSE(reverse_flag);
  TEST_ASSERT_EQUAL_FLOAT(15.8, pwm_forward);
}

void test_motor_speed_with_direction(void) {
  reverse_flag = false;
  get_kmph_ExpectAndReturn(3.2);
  TEST_ASSERT_EQUAL_FLOAT(3.2, motor_speed_with_direction());

  reverse_flag = true;
  get_kmph_ExpectAndReturn(3.2);
  TEST_ASSERT_EQUAL_FLOAT(-3.2, motor_speed_with_direction());
}

void test_get_pwm_forward(void) {
  pwm_forward = 15.9;
  TEST_ASSERT_EQUAL_FLOAT(15.9, get_pwm_forward());

  pwm_forward = 18;
  TEST_ASSERT_EQUAL_FLOAT(18, get_pwm_forward());
}
