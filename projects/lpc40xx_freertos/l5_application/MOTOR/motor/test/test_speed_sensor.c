#include "unity.h"

#include <stdio.h>

#include "Mockboard_io.h"
#include "Mockgpio.h"
#include "Mocklpc40xx.h"
#include "Mocklpc_peripherals.h"

#include "speed_sensor.c"

void setUp(void) {}

void tearDown(void) {}

void test_initialize_speed_sensor_interrupts(void) {
  gpio_s gpio_1;
  gpio__construct_as_input_ExpectAndReturn(0, 6, gpio_1);
  gpio__construct_as_input_ExpectAndReturn(0, 29, gpio_1);
  lpc_peripheral__enable_interrupt_Expect(LPC_PERIPHERAL__GPIO, interrupt_handle, NULL);
  initialize_speed_sensor_interrupts();
}

void test_get_rpm(void) {
  rpm = 120;
  TEST_ASSERT_EQUAL_UINT32(120, get_rpm());

  rpm = 80;
  TEST_ASSERT_EQUAL_UINT32(80, get_rpm());
}

void test_get_kmph(void) {
  kmph = 3.2;
  TEST_ASSERT_EQUAL_FLOAT(3.2, get_kmph());

  kmph = 5.6;
  TEST_ASSERT_EQUAL_FLOAT(5.6, get_kmph());
}

void test_calculate_rpm_and_kmph(void) {
  calculate_rpm_and_kmph(2);
  uint32_t test_rpm = (rotations * 120);
  float test_kmph = (kmph_const * rpm * wheel_diameter_in_m);
  printf("mph:%f\n", test_kmph);
  TEST_ASSERT_EQUAL_UINT32(test_rpm, get_rpm());
  TEST_ASSERT_EQUAL_FLOAT(test_kmph, get_kmph());

  calculate_rpm_and_kmph(1);
  test_rpm = (rotations * 120);
  test_kmph = (kmph_const * rpm * wheel_diameter_in_m);
  TEST_ASSERT_EQUAL_UINT32(test_rpm, get_rpm());
  TEST_ASSERT_EQUAL_FLOAT(test_kmph, get_kmph());
}

void test_clear_rotations(void) {
  rotations = 4;
  clear_rotations();

  uint32_t test_rpm = (rotations * 120);
  float test_kmph = (kmph_const * rpm * wheel_diameter_in_m);
  TEST_ASSERT_EQUAL_UINT32(test_rpm, get_rpm());
  TEST_ASSERT_EQUAL_FLOAT(test_kmph, get_kmph());

  TEST_ASSERT_EQUAL(0, rotations);
}
